package vn.tinylab.service;

import vn.tinylab.entity.Order;
import vn.tinylab.model.request.OrderRequest;

import javax.management.BadAttributeValueExpException;

public interface OrderService {

    Order orderProducts(OrderRequest request) throws BadAttributeValueExpException;
}
