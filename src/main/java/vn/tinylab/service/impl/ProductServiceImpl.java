package vn.tinylab.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import vn.tinylab.entity.Product;
import vn.tinylab.entity.Vendor;
import vn.tinylab.model.request.ProductRequest;
import vn.tinylab.repo.ProductRepository;
import vn.tinylab.repo.VendorRepository;
import vn.tinylab.service.ProductService;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final VendorRepository vendorRepository;
    private final ProductRepository productRepository;

    @Override
    public Product createProduct(ProductRequest productRequest) {
        Optional<Vendor> vendor = vendorRepository.findById(productRequest.getVendorId());
        if(!vendor.isPresent()) {
            throw new EntityExistsException(String.format("Not found vendor with ID = {}", productRequest.getVendorId()));
        }

        productRequest.setName(productRequest.getName() == null ? "" : productRequest.getName().trim());
        productRequest.setPrice(productRequest.getPrice() == null ? 0.0 : productRequest.getPrice());
        productRequest.setCurrentQuantity(productRequest.getCurrentQuantity() == null ? 0 : productRequest.getCurrentQuantity());

        Product createdProduct = new Product();
        createdProduct.setName(productRequest.getName());
        createdProduct.setPrice(productRequest.getPrice());
        createdProduct.setCurrentQuantity(productRequest.getCurrentQuantity());
        createdProduct.setVendor(vendor.get());

        createdProduct = productRepository.save(createdProduct);

        return createdProduct;
    }

    @Override
    public synchronized Product updateProduct(long existedProductId, long quantity) {
        Optional<Product> localProductOptional = productRepository.findById(existedProductId);
        if(!localProductOptional.isPresent()) {
            throw new EntityNotFoundException(String.format("Not found product with ID = {} to update it.", existedProductId));
        }

        Product updatedProduct = localProductOptional.get();
        updatedProduct.setCurrentQuantity(updatedProduct.getCurrentQuantity() + quantity);

        return productRepository.save(updatedProduct);
    }
}
