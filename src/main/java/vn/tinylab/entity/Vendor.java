package vn.tinylab.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "Vendors")
@Getter
@Setter
public class Vendor extends AbstractUser {

    @Column(columnDefinition = "SMALLINT default 1")
    private int enabled;
}
