package vn.tinylab.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity(name = "Customers")
@Getter
@Setter
public class Customer extends AbstractUser {
}
