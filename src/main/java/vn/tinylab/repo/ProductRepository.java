package vn.tinylab.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.tinylab.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
